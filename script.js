// SYOK Dashboard v2
/* 
  1) Disable Kentico form submission
  2) Define API end point
  3) Variables
  - a) IDs
  - b) URL
  - c) Category Lists
  - d) Images from undraw
  4) HTML
  - a) Kentico Bug
  - b) Loading spinner when user submitted query on search form
  - c) Loading spinner on remove button when user remove an article/episode podcast
  5) Functions
  - a) Get random image from the list above
  - b) Render random images on first load / article tab
  - c) Function to convert new date
  - d) ClearResult
  6) Fetch (GET)
  - a) Article
  - b) SYOKcast
  - c) SYOKcast Episode
  - d) List Sponsored Episode
  - e) List Filtered Keyword (profanity words)
  7) Fetch (POST)
  - a) Sponsored / unsponsored request
  - b) Edit SYOKcast request
  - c) Cancel SYOKcast request
  - d) Save SYOKcast request
  - e) Remove article request
  - f) Remove syokcast episode request
  - g) Update keyword filter
  8) Misc
  - a) Tab function
*/

// 1) Disable Kentico form submission
let kenticoForm = document.getElementById('form');
if (kenticoForm) {
  kenticoForm.addEventListener('submit', (e) => {
    e.preventDefault();
  });
}

// 2) Define API end point
let api = 'https://sfpzzwk1xi.execute-api.ap-southeast-1.amazonaws.com/api/';
let articles = api + 'articles';
let articles_approve = articles + '/approve';
let remove_article = articles + '/disable';
let sponsored_podcast = api + 'podcast/episode/sponsor';
let syokcast = api + 'podcast';
let syokcast_update = syokcast + '/update';
let remove_episode = api + 'podcast/episode/delete';
let setting_api = api + 'settings';
let setting_update = setting_api + '/update';

// 3) Variables
// - a) IDs
let formPodcast = document.getElementById('searchFormPodcast');
let formArticle = document.getElementById('searchFormArticle');
let formSyokcast = document.getElementById('searchFormSyokcast');
let listResults = document.getElementById('listResults');
let listSponsored = document.getElementById('listSponsored');

// - b) URL
let url = 'https://media2.fishtank.my/app_themes/syok/dashboard/';

// - c) Category Lists
// category list is inside category.json
// edit the category from there
// let categoryArray = '/category.json';
let categoryArray = url + 'category.json';
let siteArray = [
  'Publisher Name',
  'Rojak Daily',
  'Astro Gempak',
  'Stadium Astro',
  'Xuan',
  'Iamtravelist',
  'Ulagam',
  'Foothrottle',
  'BuzzKini',
  'DW',
  'Islam Itu Indah',
];
let statusArray = ['Status', 'Approved', 'Unapproved'];

// - d) Images from undraw
let images = [
  url + 'images/undraw-syok1.svg',
  url + 'images/undraw-syok2.svg',
  url + 'images/undraw-syok3.svg',
  url + 'images/undraw-syok4.svg',
  url + 'images/undraw-syok5.svg',
  url + 'images/undraw-syok6.svg',
  url + 'images/undraw-syok7.svg',
  url + 'images/undraw-syok8.svg',
  url + 'images/undraw-syok9.svg',
  url + 'images/undraw-syok10.svg',
  url + 'images/undraw-syok11.svg',
  url + 'images/undraw-syok12.svg',
  url + 'images/undraw-syok13.svg',
  url + 'images/undraw-syok14.svg',
  url + 'images/undraw-syok15.svg',
  url + 'images/undraw-syok16.svg',
  url + 'images/undraw-syok17.svg',
  url + 'images/undraw-syok18.svg',
  url + 'images/undraw-syok19.svg',
  url + 'images/undraw-syok20.svg',
  url + 'images/undraw-syok21.svg',
  url + 'images/undraw-syok22.svg',
  url + 'images/undraw-syok23.svg',
  url + 'images/undraw-syok24.svg',
  url + 'images/undraw-syok25.svg',
  url + 'images/undraw-syok26.svg',
];

// 4) HTML
// - a) Kentico Bug
// fixing kentico missing form tag with a fake form tag.
// unknown bug issue from kentico side
let hiddenForm = `
  <!-- solving KENTICO missing form tag with a fake form tag -->
  <form>
    <input type="hidden" name="kenticoF" value="kenticoF">
  </form>`;

// - b) Loading spinner when user submitted query on search form
let spinner = `
  <div class="d-flex justify-content-center align-items-center" style="height:200px;opacity: 0.3;">
    <div class="spinner-border" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>`;

// - c) Loading spinner on remove button when user remove an article/episode podcast
let loadingBtn =
  '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="sr-only">Loading</span>';

// 5) Functions
// - a) Get random image from the list above
function randomImages() {
  let randomItem = images[Math.floor(Math.random() * images.length)];
  listResults.innerHTML =
    '<div class="row justify-content-center mt-4"><div class="col-4"><img src="' +
    randomItem +
    '" class="img-fluid" style="opacity: 0.7"/></div></div>';
}
// - b) Render random images on first load / article tab
// randomImages();

// - c) Function to convert new date
function newDateFormat(newDate) {
  let date = new Date(newDate);
  let months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  let dateString =
    ('00' + date.getDate()).slice(-2) +
    ' ' +
    months[date.getMonth()] +
    ' ' +
    date.getFullYear() +
    ' | ' +
    ('00' + date.getHours()).slice(-2) +
    ':' +
    ('00' + date.getMinutes()).slice(-2) +
    ':' +
    ('00' + date.getSeconds()).slice(-2);

  return dateString;
}

// - d) ClearResult
function clearResult() {
  document.getElementById('listResults').innerHTML = '';
}

// 6) Fetch (GET)
// - a) Article
// disable form on submit
if (formArticle) {
  formArticle.addEventListener('submit', (e) => {
    e.preventDefault();
    listResults.innerHTML = spinner;
    searchArticleApi();
  });
}
// search function for article
function searchArticleApi() {
  let val = document.getElementById('searchFormInputArticle').value;
  console.log('Searching for ' + val);
  fetch(api + 'articles/search?title=' + val)
    .then((resp) => resp.json())
    // if success
    .then((data) => {
      console.log('👍🏻:', data);
      if (data.length === 0) {
        let output = `
          <div class="row justify-content-center mt-4">
            <div class="col-4 text-center">
              <h5 class="my-4">No result</h5>
              <img src="images/undraw-emptyresult.svg" class="img-fluid" style="opacity: 0.4;filter:grayscale(1);">
            </div>
          </div>
          `;
        listResults.innerHTML = output;
      } else {
        let output =
          '<div class="row row-search-title"><div class="col-10"><h3>Search result(s) for ' +
          val +
          ' > ' +
          data.length +
          '</h3>' +
          '</div><div class="col-2 text-center"><a onClick="clearResult()" class="button button-clear">Clear</a></div></div>' +
          "<div class='row'><div class='col-12'>" +
          "<div class='form-group'>" +
          "<input type='text' id='search' class='form-control' placeholder='Refine your search by title or description..'>" +
          '</div>' +
          '</div>' +
          "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div></div>";
        data.forEach((post) => {
          // console.log(post);
          output += hiddenForm;
          output += `
        <div class="row row-article">
          <div class="col-12">
            <div class="feed-list-item" id="${post.ID}">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                ${
                  post.LandscapeImage !== null && post.LandscapeImage !== ''
                    ? `<img src="${post.LandscapeImage}" class="img-fluid"></img>`
                    : `<img src="https://dummyimage.com/640x360/ff0000/fff.jpg&text=No+image!" class="img-fluid"></img>`
                }
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Name}</h5>
                  <p>${post.Description}</p>
                  <span class="badge badge-dark">${
                    post.Sitename
                  }</span> <span class="badge badge-dark">${newDateFormat(
            post.DocumentPublishFrom
          )}<span>                
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                  <a data-toggle="modal" data-target="#confirmRemove-${
                    post.ID
                  }" class="button button-remove">Remove</a>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="confirmRemove-${
            post.ID
          }" tabindex="-1" role="dialog" aria-labelledby="confirmRemove-${
            post.ID
          }Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm">
              <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <img src="https://media.giphy.com/media/xT5LMDYzIZgZ0M9KdG/giphy.gif" class="img-fluid">
                  <br />
                  <p>Are you sure you want to delete this article? This action can't be undone.</p>
                  <div class="remove-detail">
                    <div class="remove-wrap">
                      <span>Title:</span>
                      <p>${post.Name}</p>
                    </div>
                    <div class="remove-wrap">
                      <span>Date:</span>
                      <p>${newDateFormat(post.DocumentPublishFrom)}</p>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button id="cancelBtn-${
                    post.ID
                  }" type="button" class="btn btn-secondary d-inline-block" data-dismiss="modal">Cancel</button>
                  <button id="submitBtn-${
                    post.ID
                  }" type="button" class="btn button-remove d-inline-block" onClick="removeArticle(${
            post.ID
          })">Remove</button>
                </div>
              </div>
            </div>
          </div>
        </div>`;
        });

        document.getElementById('listResults').innerHTML = output;
        //filtering function on the search result
        document
          .getElementById('search')
          .addEventListener('keyup', searchArticle);

        function searchArticle() {
          let value = document.getElementById('search').value;
          data.forEach((post) => {
            let title = post.Name.toLowerCase();
            let description = post.Description.toLowerCase();
            let publisher = post.Sitename.toLowerCase();
            if (
              post.ID == value ||
              title.indexOf(value) > -1 ||
              description.indexOf(value) > -1 ||
              publisher.indexOf(value) > -1 ||
              value == ''
            ) {
              document.getElementById(post.ID).style.display = 'block';
            } else {
              document.getElementById(post.ID).style.display = 'none';
            }
          });
        }
      }
    })
    // if err
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}

// - b) SYOKcast
// disable form on submit
if (formSyokcast) {
  formSyokcast.addEventListener('submit', (e) => {
    e.preventDefault();
    listResults.innerHTML = spinner;
    searchSyokcast();
  });
}
// search SYOKcast function
function searchSyokcast() {
  let val = document.getElementById('searchFormInputSyokcast').value;
  fetch(syokcast + '/search?title=' + val)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log('👍🏻:', data);
      if (data.length === 0) {
        let output = `
          <div class="row justify-content-center mt-4">
            <div class="col-4 text-center">
              <h5 class="my-4">No result</h5>
              <img src="images/undraw-emptyresult.svg" class="img-fluid" style="opacity: 0.4;filter:grayscale(1);">
            </div>
          </div>
          `;
        listResults.innerHTML = output;
      } else {
        let output =
          '<div class="row row-search-title"><div class="col-10"><h3>Search result(s) for ' +
          val +
          ' > ' +
          data.length +
          '</h3>' +
          '</div><div class="col-2 text-center"><a onClick="clearResult()" class="button button-clear">Clear</a></div></div>' +
          "<div class='row'>" +
          "<div class='col-12'>" +
          "<div class='form-group'>" +
          "<input type='text' id='search' class='form-control' placeholder='Refine your search by title or description..'>" +
          '</div>' +
          '</div>' +
          "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div></div>";
        data.forEach((post) => {
          output += hiddenForm;
          output += `
        <div class="row row-syokcast">
          <div class="col-12 display-item display-item-show" id="display-${
            post.PlaylistId
          }">
            <div class="feed-list-item">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                  <img src="${post.Image}" alt="" class="img-fluid">
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Title}</h5>
                  <p>${post.Description}</p>
                  <span class="badge badge-dark">${
                    post.Category
                  }</span> <span class="badge badge-dark">${
            post.Language
          }</span> <span class="badge badge-dark">${
            post.Syokcast == 0 ? 'External' : 'Syokcast'
          }</span> <span class="badge badge-dark">${
            post.Sponsored == 0 ? 'Not sponsored' : 'Sponsored'
          }</span>
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                  <a onClick="editApi(${
                    post.PlaylistId
                  })" class="button button-edit">Edit</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 edit-item edit-item-hidden" id="edit-${
            post.PlaylistId
          }">
            <form id="editFormSyokcast-${post.PlaylistId}">
              <input type="hidden" name="podcast_id" value="${post.PlaylistId}">
              <div class="feed-list-item">
                <div class="row feed-card justify-content-center">
                  <div class="col-2 feed-card-image">
                    <img src="${post.Image}" alt="" class="img-fluid">
                  </div>
                  <div class="col-8 feed-card-title">
                    <h5>${post.Title}</h5>
                    <p>Description</p>
                    <textarea class="my-1 mr-sm-2 form-control" row="7" name="description">${
                      post.Description
                    }</textarea>
                    <div class="row">
                      <div class="col-4">
                        <p>Category</p>
                          <select class="my-1 mr-sm-2 form-control" name="category" id="categoryList-${
                            post.PlaylistId
                          }">
                          </select>
                      </div>
                      <div class="col-4">
                        <p>Language</p>
                          <select class="my-1 mr-sm-2 form-control" name="lang" id="language-${
                            post.PlaylistId
                          }">
                            <option value=''></option>
                            <option value='malay' ${
                              post.Language == 'malay' ? 'selected' : ''
                            }>Malay</option>
                            <option value='english' ${
                              post.Language == 'english' ? 'selected' : ''
                            }>English</option>
                            <option value='chinese' ${
                              post.Language == 'chinese' ? 'selected' : ''
                            }>Chinese</option>
                            <option value='tamil' ${
                              post.Language == 'tamil' ? 'selected' : ''
                            }>Tamil</option>
                          </select>
                      </div>
                      <div class="col-2">
                        <p>Syokcast</p>
                          <select class="my-1 mr-sm-2 form-control" name="syokcast" id="syokcast-${
                            post.PlaylistId
                          }">
                            <option value="0" ${
                              post.Syokcast === 0 ? 'selected' : ''
                            }>No</option>
                            <option value="1" ${
                              post.Syokcast === 1 ? 'selected' : ''
                            }>Yes</option>
                          </select>
                      </div>
                      <div class="col-2">
                        <p>Sponsored</p>
                          <select class="my-1 mr-sm-2 form-control" name="sponsored" id="sponsored-${
                            post.PlaylistId
                          }">
                            <option value="0" ${
                              post.Sponsored == 0 ? 'selected' : ''
                            }>No</option>
                            <option value="1" ${
                              post.Sponsored == 1 ? 'selected' : ''
                            }>Yes</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-2 feed-card-button text-center align-self-center">
                    <button onClick="saveApi(${
                      post.PlaylistId
                    })" class="button button-save" type="submit" id="submit-${
            post.PlaylistId
          }">Save</button><br />
                    <button onClick="cancelApi(${
                      post.PlaylistId
                    })" class="button button-remove" type="reset" id="reset-${
            post.PlaylistId
          }">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>`;
        });
        document.getElementById('listResults').innerHTML = output;
        //filtering function
        document
          .getElementById('search')
          .addEventListener('keyup', searchArticle);

        function searchArticle() {
          let value = document.getElementById('search').value;
          data.forEach((post) => {
            let title = post.Title.toLowerCase();
            let description = post.Description.toLowerCase();
            if (
              post.PlaylistId == value ||
              title.indexOf(value) > -1 ||
              description.indexOf(value) > -1 ||
              value == ''
            ) {
              document.getElementById(
                'display-' + post.PlaylistId
              ).style.display = 'block';
            } else {
              document.getElementById(
                'display-' + post.PlaylistId
              ).style.display = 'none';
            }
          });
        }
        // create Select element from category.json
        data.forEach((post) => {
          let selectCategory = document.getElementById(
            'categoryList-' + post.PlaylistId
          );
          fetch(categoryArray)
            .then((resp) => resp.json())
            // if success
            .then((data) => {
              let optg;
              let el;
              for (let i = 0; i < data.length; i++) {
                // create Main Category from JSON
                if (data[i].hasOwnProperty('label')) {
                  optg = document.createElement('optgroup');
                  optg.label = data[i].label;
                  // create Sub Category from JSON
                  if (data[i].hasOwnProperty('options')) {
                    for (let x = 0; x < data[i].options.length; x++) {
                      el = document.createElement('option');
                      el.text = data[i].options[x];
                      el.value = data[i].options[x];
                      if (data[i].options[x] == post.Category) {
                        el.selected = true;
                        el.setAttribute('selected', 'selected');
                      }
                      optg.appendChild(el);
                    }
                  }
                  selectCategory.appendChild(optg);
                }
              }
            });
        });
      }
    })
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}

// - c) SYOKcast Episode
// disable form on submit
if (formPodcast) {
  formPodcast.addEventListener('submit', (e) => {
    e.preventDefault();
    listResults.innerHTML = spinner;
    searchPodcastApi();
  });
}
// search Podcast Episode
function searchPodcastApi() {
  let val = document.getElementById('searchFormInputPodcast').value;
  // console.log('Searching for ' + val);
  fetch(api + 'podcast/episode/search?title=' + val)
    .then((resp) => resp.json())
    // if success
    .then((data) => {
      // console.log('👍🏻:', data);
      if (data.length === 0) {
        let output = `
          <div class="row justify-content-center mt-4">
            <div class="col-4 text-center">
              <h5 class="my-4">No result</h5>
              <img src="images/undraw-emptyresult.svg" class="img-fluid" style="opacity: 0.4;filter:grayscale(1);">
            </div>
          </div>
          `;
        listResults.innerHTML = output;
      } else {
        let output =
          '<div class="row row-search-title"><div class="col-10"><h3>Search result(s) for ' +
          val +
          ' > ' +
          data.length +
          '</h3>' +
          '</div><div class="col-2 text-center"><a onClick="clearResult()" class="button button-clear">Clear</a></div></div>' +
          "<div class='row'>" +
          "<div class='col-12'>" +
          "<div class='form-group'>" +
          "<input type='text' id='search' class='form-control' placeholder='Refine your search by title or description..'>" +
          '</div>' +
          '</div>' +
          "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div></div>";
        data.forEach((post) => {
          // console.log(post);
          output += `
        <div class="row row-episode">
          <div class="col-12">
            <div class="feed-list-item" id="${post.EpisodeId}">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                  <img src="${post.Image}" alt="" class="img-fluid">
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Title}</h5>
                  <p>${post.Description}</p>
                  <span class="badge badge-dark">${post.PodcastTitle}</span>
                  <span class="badge badge-dark">${newDateFormat(
                    post.PublishedAt
                  )}</span>
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                  <a onClick="sponsorApi(${
                    post.EpisodeId
                  }, 1)" class="button button-sponsor">Sponsor</a> <br />
                  <a data-toggle="modal" data-target="#confirmRemove-${
                    post.EpisodeId
                  }" class="button button-remove">Remove</a>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="confirmRemove-${
            post.EpisodeId
          }" tabindex="-1" role="dialog" aria-labelledby="confirmRemove-${
            post.EpisodeId
          }Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm">
              <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <img src="https://media.giphy.com/media/xT5LMDYzIZgZ0M9KdG/giphy.gif" class="img-fluid">
                  <br />
                  <p>Are you sure you want to delete this episode? This action can't be undone.</p>
                  <div class="remove-detail">
                    <div class="remove-wrap">
                      <span>Title:</span>
                      <p>${post.Title}</p>
                    </div>
                    <div class="remove-wrap">
                      <span>Date:</span>
                      <p>${newDateFormat(post.PublishedAt)}</p>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary d-inline-block" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn button-remove d-inline-block" onClick="removeEpisode(${
                    post.EpisodeId
                  })" id="btn-${post.EpisodeId}">Remove</button>
                </div>
              </div>
            </div>
          </div>
        </div>`;
        });

        document.getElementById('listResults').innerHTML = output;
        //filtering function
        document
          .getElementById('search')
          .addEventListener('keyup', searchArticle);

        function searchArticle() {
          let value = document.getElementById('search').value;
          data.forEach((post) => {
            let title = post.Title.toLowerCase();
            let description = post.Description.toLowerCase();
            if (
              post.EpisodeId == value ||
              title.indexOf(value) > -1 ||
              description.indexOf(value) > -1 ||
              value == ''
            ) {
              document.getElementById(post.EpisodeId).style.display = 'block';
            } else {
              document.getElementById(post.EpisodeId).style.display = 'none';
            }
          });
        }
      }
    })
    // if err
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}

// - d) List Sponsored Episode
// list down sponsored episode on the episode tab
function getSponsored() {
  fetch(api + 'podcast/episode/sponsored')
    .then((response) => response.json())
    .then((data) => {
      let resultHead =
        "<h2 class='mb-0'><button class='btn btn-block text-justify' type='button' data-toggle='collapse' data-target='#sponsoredList' aria-expanded='true' aria-controls='sponsoredList'>" +
        data.length +
        " Sponsored Episode(s) <i class='fas fa-angle-double-down'></i></button></h2>";
      document.getElementById('sponsoredHead').innerHTML = resultHead;
      let output =
        "<div class='testing'>" +
        "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div>";
      data.forEach((post) => {
        // console.log(post);
        output += `
          <div class="col-12">
            <div class="feed-list-item" id="${post.EpisodeId}">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                  <img src="${post.Image}" alt="" class="img-fluid">
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Title}</h5>
                  <p>${post.Description}</p>
                  <span class="badge badge-dark">${post.PodcastTitle}</span>
                  <span class="badge badge-dark">${newDateFormat(
                    post.PublishedAt
                  )}</span>
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                  <a onClick="sponsorApi(${
                    post.EpisodeId
                  }, 0)" class="button button-unsponsor">Unsponsor</a>
                </div>
              </div>
            </div>
          </div>`;
      });

      document.getElementById('listSponsored').innerHTML = output;
    });
}

// - e) List Filtered Keyword (profanity words)
// get the keywords for profanity filtering
function getKeyword() {
  document.getElementById('btnKeyword').className =
    'btn btn-light d-block w-100 mb-2 ';
  document.getElementById('btnKeyword').innerHTML = loadingBtn;
  fetch(setting_api + '?type=profanity_words')
    .then((resp) => {
      return resp.json();
    })
    // if success
    .then((data) => {
      // console.log('👍🏻:', data);
      document.getElementById('btnKeyword').className =
        'btn btn-primary d-block w-100 mb-2';
      document.getElementById('btnKeyword').innerHTML = 'Save';
      data.forEach((post) => {
        let keyword = post.Values;
        $('#valueKeyword').tagsinput('add', keyword);
      });
    })
    // if err
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}

// - f) List all the approved article today
// get listing for all the approved article for the day
function getApprovedArticle() {
  // adding option approved / unapproved
  for (let i = 0; i < statusArray.length; i++) {
    let select = document.getElementById('statusDD');
    let option = document.createElement('option');
    option.text = statusArray[i];
    option.value = statusArray[i].toLowerCase();
    select.appendChild(option);
    select.options[0].disabled = true;
  }

  // adding option publisher
  for (let x = 0; x < siteArray.length; x++) {
    let select = document.getElementById('publisherDD');
    let option = document.createElement('option');
    option.text = siteArray[x];
    option.value = siteArray[x].toLowerCase();
    select.appendChild(option);
    select.options[0].disabled = true;
  }

  let statusDD = document.getElementById('statusDD');
  let publisherDD = document.getElementById('publisherDD');
  let filterForm = document.getElementById('filterForm');
  filterForm.addEventListener('submit', (e) => {
    e.preventDefault();
    listResults.innerHTML = spinner;
    getFilterArticle();
  });

  function getFilterArticle() {
    let status = statusDD.options[statusDD.selectedIndex].value;
    let publisher = publisherDD.options[publisherDD.selectedIndex].value;
    let publisherTXT = publisherDD.options[publisherDD.selectedIndex].text;
    fetch(
      articles +
        '?status=' +
        status +
        '&publisher=' +
        publisher +
        '&start=1&limit=25'
    )
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        let output = '';
        // no article published to day
        if (data.length === 0) {
          output += `
          <div class="row justify-content-center mt-4">
            <div class="col-4 text-center">
              <h5 class="my-4">No result</h5>
              <img src="images/undraw-emptyresult.svg" class="img-fluid" style="opacity: 0.4;filter:grayscale(1);">
            </div>
          </div>
          `;
          listResults.innerHTML = output;
        }
        // got article published today
        else {
          output +=
            '<div class="row mt-4"><div class="col-12"><h3>' +
            publisherTXT +
            ' article : ' +
            data.length +
            '</h3></div></div>' +
            "<div class='row'>" +
            "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div></div>";
          data.forEach((post) => {
            output += `
          <div class="row row-article">
          <div class="col-12">
            <div class="feed-list-item" id="${post.ID}">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                ${
                  post.LandscapeImage !== null && post.LandscapeImage !== ''
                    ? `<img src="${post.LandscapeImage}" class="img-fluid"></img>`
                    : `<img src="https://dummyimage.com/640x360/ff0000/fff.jpg&text=No+image!" class="img-fluid"></img>`
                }
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Name}</h5>
                  <span class="badge badge-dark">${newDateFormat(
                    post.DocumentPublishFrom
                  )}</span>
                  ${
                    status === 'approved'
                      ? ``
                      : `<br />
                  <span class="mt-2 badge badge-warning">${post.Keyword}</span>`
                  }
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                    ${
                      status === 'approved'
                        ? `<span style="opacity:.2;">No Action</span>`
                        : `<a data-toggle="modal" data-target="#confirmApprove-${post.ID}" class="button button-save">Approve</a>
                        <a data-toggle="modal" data-target="#confirmDisapprove-${post.ID}" class="button button-remove mb-0">Disapprove</a>`
                    }
                </div>
              </div>
            </div>
            ${
              status === 'approved'
                ? ``
                : `
                <div class="modal fade" id="confirmApprove-${
                  post.ID
                }" tabindex="-1" role="dialog" aria-labelledby="confirmApprove-${
                    post.ID
                  }Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm">
              <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <img src="https://media.giphy.com/media/dC9GjF8XkHXOUS7QY8/giphy.gif" class="img-fluid">
                  <br />
                  <p>Are you sure you want to approve this article?</p>
                  <div class="remove-detail">
                    <div class="remove-wrap">
                      <span>Title:</span>
                      <p>${post.Name}</p>
                    </div>
                    <div class="remove-wrap">
                      <span>Date:</span>
                      <p>${newDateFormat(post.DocumentPublishFrom)}</p>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button id="cancelBtn-${
                    post.ID
                  }" type="button" class="btn btn-secondary d-inline-block" data-dismiss="modal">Cancel</button>
                  <button id="submitBtn-${
                    post.ID
                  }" type="button" class="btn button-remove d-inline-block" onClick="approveArticle(${
                    post.ID
                  })">Approve</button>
                </div>
              </div>
            </div>
          </div>
              <div class="modal fade" id="confirmDisapprove-${
                post.ID
              }" tabindex="-1" role="dialog" aria-labelledby="confirmDisapprove-${
                    post.ID
                  }Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm">
              <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <img src="https://media.giphy.com/media/kEo43eHN3oOoI5Ub4V/giphy.gif" class="img-fluid">
                  <br />
                  <p>Are you sure you want to disapprove this article?</p>
                  <div class="remove-detail">
                    <div class="remove-wrap">
                      <span>Title:</span>
                      <p>${post.Name}</p>
                    </div>
                    <div class="remove-wrap">
                      <span>Date:</span>
                      <p>${newDateFormat(post.DocumentPublishFrom)}</p>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button id="cancelBtn-${
                    post.ID
                  }" type="button" class="btn btn-secondary d-inline-block" data-dismiss="modal">Cancel</button>
                  <button id="submitBtn-${
                    post.ID
                  }" type="button" class="btn button-remove d-inline-block" onClick="disapproveArticle(${
                    post.ID
                  })">Disapprove</button>
                </div>
              </div>
            </div>
          </div>
              `
            }
          </div>
          </div>`;
          });
          listResults.innerHTML = output;
        }
      });
  }
}
getApprovedArticle();

// 7) Fetch (POST)
// - a) Sponsored / unsponsored request
function sponsorApi(id, sponsor) {
  let fetchData = {
    method: 'POST',
    body: JSON.stringify({ episode_id: id, sponsored: sponsor }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  fetch(sponsored_podcast, fetchData)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      getSponsored();
      searchPodcastApi();
    })
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}

// - b) Edit SYOKcast request
function editApi(id) {
  // hide display
  $('#display-' + id)
    .removeClass('display-item-show')
    .addClass('display-item-hidden');
  // show edit
  $('#edit-' + id)
    .removeClass('edit-item-hidden')
    .addClass('edit-item-show');
}

// - c) Cancel SYOKcast request
function cancelApi(id) {
  // hide display
  $('#display-' + id)
    .removeClass('display-item-hidden')
    .addClass('display-item-show');
  // show edit
  $('#edit-' + id)
    .removeClass('edit-item-show')
    .addClass('edit-item-hidden');
  document.getElementById('reset-' + id).addEventListener('reset', (e) => {
    e.preventDefault();
  });
}

// - d) Save SYOKcast request
function saveApi(id) {
  // hide display
  $('#display-' + id)
    .removeClass('display-item-hidden')
    .addClass('display-item-show');
  // show edit
  $('#edit-' + id)
    .removeClass('edit-item-show')
    .addClass('edit-item-hidden');

  let editSyokcast = document.getElementById('editFormSyokcast-' + id);
  editSyokcast.addEventListener('submit', (e) => {
    e.preventDefault();
    let formData = new FormData(editSyokcast);
    var object = {};
    formData.forEach((value, key) => {
      object[key] = value;
    });
    var json = JSON.stringify(object);
    let fetchData = {
      method: 'POST',
      body: json,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
    fetch(syokcast_update, fetchData)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        searchSyokcast();
      })
      .catch((error) => {
        console.error('👎🏻 : ', error);
      });
  });
}

// - e) Remove article request
function removeArticle(id) {
  let $modal = $('#confirmRemove-' + id);
  let fetchData = {
    method: 'POST',
    body: JSON.stringify({ article_id: id }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  document.getElementById('submitBtn-' + id).innerHTML = loadingBtn;
  fetch(remove_article, fetchData)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      $modal.modal('hide');
      listResults.innerHTML = spinner;
      searchArticleApi();
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}

// - f) Remove syokcast episode request
function removeEpisode(id) {
  let $modal = $('#confirmRemove-' + id);
  let fetchData = {
    method: 'POST',
    body: JSON.stringify({ episode_id: id }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  document.getElementById('btn-' + id).innerHTML = loadingBtn;
  fetch(remove_episode, fetchData)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      $modal.modal('hide');
      listResults.innerHTML = spinner;
      searchPodcastApi();
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}

// - g) Update keyword filter
function saveKeyword() {
  let formKey = document.getElementById('formKeyword');
  formKey.addEventListener('submit', (e) => {
    e.preventDefault();
    let formData = new FormData(formKey);
    var object = {};
    formData.forEach((value, key) => {
      object[key] = value;
    });
    var json = JSON.stringify(object);
    let fetchData = {
      method: 'POST',
      body: json,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
    document.getElementById('btnKeyword').className =
      'btn btn-success d-block w-100 mb-2';
    document.getElementById('btnKeyword').innerHTML = 'Saved';
    document.getElementById('btnKeyword').disabled = true;
    fetch(setting_update, fetchData)
      .then((response) => {
        return response.json();
      })
      .then(() => {
        document.getElementById('btnKeyword').className =
          'btn btn-primary d-block w-100 mb-2';
        document.getElementById('btnKeyword').innerHTML = 'Save';
        document.getElementById('btnKeyword').disabled = false;
      })
      .catch((error) => {
        console.error('👎🏻 : ', error);
      });
  });
}

// - h) Disapprove article request
function disapproveArticle(id) {
  let $modal = $('#confirmDisapprove-' + id);
  let fetchData = {
    method: 'POST',
    body: JSON.stringify({ article_id: id, status: 2 }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  document.getElementById('submitBtn-' + id).innerHTML = loadingBtn;
  fetch(articles_approve, fetchData)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      $modal.modal('hide');
      listResults.innerHTML = spinner;
      getApprovedArticle();
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}

// - h) Disapprove article request
function approveArticle(id) {
  let $modal = $('#confirmApprove-' + id);
  let fetchData = {
    method: 'POST',
    body: JSON.stringify({ article_id: id, status: 1 }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  document.getElementById('submitBtn-' + id).innerHTML = loadingBtn;
  fetch(articles_approve, fetchData)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      $modal.modal('hide');
      listResults.innerHTML = spinner;
      getApprovedArticle();
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}

// 8) Misc
// - a) tab function
// article tab showing
$('#article-tab').on('shown.bs.tab', function (e) {
  randomImages();
});
// article tab hidden
$('#article-tab').on('hidden.bs.tab', function (e) {
  clearResult();
});

// syokcast tab hidden
$('#syokcast-tab').on('shown.bs.tab', function (e) {
  randomImages();
});
// syokcast tab hidden
$('#syokcast-tab').on('hidden.bs.tab', function (e) {
  clearResult();
});

// podcast tab showing
$('#podcast-tab').on('shown.bs.tab', function (e) {
  document.getElementById('rowSponsored').style.display = 'block';
  document.getElementById('listSponsored').style.display = 'block';
  getSponsored();
  randomImages();
});
// podcast tab hidden
$('#podcast-tab').on('hidden.bs.tab', function (e) {
  document.getElementById('rowSponsored').style.display = 'none';
  document.getElementById('listSponsored').style.display = 'none';
  clearResult();
});

/* hidden feature */
// let getSyokcastBtn = document.getElementById('getSyokcastBtn');
// getSyokcastBtn.addEventListener('click', (e) => {
//   getSyokcastBtn.style.display = 'none';
// });

// - b) SYOKcast
// get list of SYOKcast
// for now we hide this function
/*
function getSyokcast() {
  fetch(syokcast + '?start=1&limit=5')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log('👍🏻:', data);
      let output =
        '<div class="row row-search-title"><div class="col-10"><h3>Search result(s) for ' +
        val +
        ' > ' +
        data.length +
        '</h3>' +
        '</div><div class="col-2 text-center"><a onClick="clearResult()" class="button button-clear">Clear</a></div></div>' +
        "<div class='row'>" +
        "<div class='col-12'>" +
        "<div class='form-group'>" +
        "<input type='text' id='search' class='form-control' placeholder='Refine your search by title or description..'>" +
        '</div>' +
        '</div>' +
        "<div class='col-12'><ul class='list-unstyled'><li class='feed-list-head'><div class='row justify-content-center align-items-center'><div class='col-2'><h6>Image</h6></div><div class='col-8'><h6>Title & Description</h6></div><div class='col-2 text-center'><h6>Action</h6></div></div></li></ul></div></div>";
      data.forEach((post) => {
        output += hiddenForm;
        output += `
        <div class="row row-syokcast">
          <div class="col-12 display-item display-item-show" id="display-${
            post.PlaylistId
          }">
            <div class="feed-list-item">
              <div class="row feed-card justify-content-center">
                <div class="col-2 feed-card-image">
                  <img src="${post.Image}" alt="" class="img-fluid">
                </div>
                <div class="col-8 feed-card-title">
                  <h5>${post.Title}</h5>
                  <p>${post.Description}</p>
                  <span class="badge badge-dark">${
                    post.Category
                  }</span> <span class="badge badge-dark">${
          post.Language
        }<span> <span class="badge badge-dark">${
          post.Syokcast == 0 ? 'External' : 'Syokcast'
        }</span> <span class="badge badge-dark">${
          post.Sponsored == 0 ? 'Not sponsored' : 'Sponsored'
        }<span>
                </div>
                <div class="col-2 feed-card-button text-center align-self-center">
                  <a onClick="editApi(${
                    post.PlaylistId
                  })" class="button button-edit">Edit</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 edit-item edit-item-hidden" id="edit-${
            post.PlaylistId
          }">
            <form id="editFormSyokcast-${post.PlaylistId}">
              <input type="hidden" name="podcast_id" value="${post.PlaylistId}">
              <div class="feed-list-item">
                <div class="row feed-card justify-content-center">
                  <div class="col-2 feed-card-image">
                    <img src="${post.Image}" alt="" class="img-fluid">
                  </div>
                  <div class="col-8 feed-card-title">
                    <h5>${post.Title}</h5>
                    <p>Description</p>
                    <textarea class="my-1 mr-sm-2 form-control" row="7" name="description">${
                      post.Description
                    }</textarea>
                    <div class="row">
                      <div class="col-4">
                        <p>Category</p>
                          <select class="my-1 mr-sm-2 form-control" name="category" id="categoryList-${
                            post.PlaylistId
                          }">
                          </select>
                      </div>
                      <div class="col-4">
                        <p>Language</p>
                          <select class="my-1 mr-sm-2 form-control" name="lang" id="language-${
                            post.PlaylistId
                          }">
                            <option value=''></option>
                            <option value='malay' ${
                              post.Language == 'malay' ? 'selected' : ''
                            }>Malay</option>
                            <option value='english' ${
                              post.Language == 'english' ? 'selected' : ''
                            }>English</option>
                            <option value='chinese' ${
                              post.Language == 'chinese' ? 'selected' : ''
                            }>Chinese</option>
                            <option value='tamil' ${
                              post.Language == 'tamil' ? 'selected' : ''
                            }>Tamil</option>
                          </select>
                      </div>
                      <div class="col-2">
                        <p>Syokcast</p>
                          <select class="my-1 mr-sm-2 form-control" name="syokcast" id="syokcast-${
                            post.PlaylistId
                          }">
                            <option value="0" ${
                              post.Syokcast === 0 ? 'selected' : ''
                            }>No</option>
                            <option value="1" ${
                              post.Syokcast === 1 ? 'selected' : ''
                            }>Yes</option>
                          </select>
                      </div>
                      <div class="col-2">
                        <p>Sponsored</p>
                          <select class="my-1 mr-sm-2 form-control" name="sponsored" id="sponsored-${
                            post.PlaylistId
                          }">
                            <option value="0" ${
                              post.Sponsored == 0 ? 'selected' : ''
                            }>No</option>
                            <option value="1" ${
                              post.Sponsored == 1 ? 'selected' : ''
                            }>Yes</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-2 feed-card-button text-center align-self-center">
                    <button onClick="saveApi(${
                      post.PlaylistId
                    })" class="button button-save" type="submit" id="submit-${
          post.PlaylistId
        }">Save</button><br />
                    <button onClick="cancelApi(${
                      post.PlaylistId
                    })" class="button button-remove" type="reset" id="reset-${
          post.PlaylistId
        }">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>`;
      });
      document.getElementById('listResults').innerHTML = output;
      //filtering function
      document
        .getElementById('search')
        .addEventListener('keyup', searchArticle);

      function searchArticle() {
        let value = document.getElementById('search').value;
        data.forEach((post) => {
          let title = post.Title.toLowerCase();
          let description = post.Description.toLowerCase();
          if (
            post.PlaylistId == value ||
            title.indexOf(value) > -1 ||
            description.indexOf(value) > -1 ||
            value == ''
          ) {
            document.getElementById(post.PlaylistId).style.display = 'block';
          } else {
            document.getElementById(post.PlaylistId).style.display = 'none';
          }
        });
      }

      // create Select element from category.json
      data.forEach((post) => {
        let selectCategory = document.getElementById(
          'categoryList-' + post.PlaylistId
        );
        fetch(categoryArray)
          .then((resp) => resp.json())
          // if success
          .then((data) => {
            let optg;
            let el;
            for (let i = 0; i < data.length; i++) {
              // create Main Category from JSON
              if (data[i].hasOwnProperty('label')) {
                optg = document.createElement('optgroup');
                optg.label = data[i].label;
                // create Sub Category from JSON
                if (data[i].hasOwnProperty('options')) {
                  for (let x = 0; x < data[i].options.length; x++) {
                    el = document.createElement('option');
                    el.text = data[i].options[x];
                    el.value = data[i].options[x];
                    if (data[i].options[x] == post.Category) {
                      el.selected = true;
                      el.setAttribute('selected', 'selected');
                    }
                    optg.appendChild(el);
                  }
                }
                selectCategory.appendChild(optg);
              }
            }
          });
      });
    })
    .catch((error) => {
      console.error('👎🏻:', error);
    });
}
*/
