var gulp = require('gulp'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  cssmin = require('gulp-cssnano'),
  prefix = require('gulp-autoprefixer'),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  // sassLint = require('gulp-sass-lint'),
  sourcemaps = require('gulp-sourcemaps'),
  // concat = require('gulp-concat'),
  // uglify = require('gulp-uglify'),
  browserSync = require('browser-sync').create();

var versionDate = new Date(); // javascript, just

var displayError = function (error) {
  // Initial building up of the error
  var errorString = '[' + error.plugin.error.bold + ']';
  errorString += ' ' + error.message.replace('\n', ''); // Removes new line at the end

  // If the error contains the filename or line number add it to the string
  if (error.fileName) errorString += ' in ' + error.fileName;

  if (error.lineNumber) errorString += ' on line ' + error.lineNumber.bold;

  // This will output an error like the following:
  // [gulp-sass] error message in file_name on line 1
  console.error(errorString);
};

var onError = function (err) {
  notify.onError({
    title: 'Gulp',
    subtitle: 'Failure!',
    message: 'Error: <%= error.message %>',
    sound: 'Basso',
  })(err);
  this.emit('end');
};

// SASS build
var sassCom = {
  outputStyle: 'compressed',
};

var sassNest = {
  outputStyle: 'nested',
};

// BUILD SUBTASKS
// ---------------

// Run :
// gulp app
// for main structure sass of the website
gulp.task('style', function () {
  return gulp
    .src('sass/stylesheet.scss')
    .pipe(
      plumber({
        errorHandler: onError,
      })
    )
    .pipe(sourcemaps.init())
    .pipe(sass(sassNest))
    .pipe(rename('style.css'))
    .pipe(gulp.dest('css'))
    .pipe(sass(sassCom))
    .pipe(cssmin())
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream());
});

// Run :
// gulp watch
gulp.task('watch', function () {
  browserSync.init({
    server: './',
  });
  gulp.watch('./*.js').on('change', browserSync.reload);
  gulp.watch('./*.html').on('change', browserSync.reload);
  gulp.watch('./sass/**/*.scss', gulp.series(['style']));
});
